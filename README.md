# README #

### NOTAS DEL DEMO ###

- El juego consiste en recolectar la mayor cantidad de monedas posibles dentro del tiempo estimado. 

- El mapa es no es lineal, es mas no tiene ruta establecida, no obstante todo punto es accesible por distintas rutas.

- Las variables que solicitan ser modificadas del inspector se encuentran en el objeto DataManager, ubicado en la escena “Splash”,
	es necesario pasar por esa escena para poder tener el encargado de recolectar datos de servidor, además de poder ser reemplazadas en el juego. Esta petición se realiza cada 1 segundo (tiempo modificable en el inspector del mismo objeto.

- Incluye minimapa general del nivel, el punto rojo indicará siempre la posición del jugador.

- Hay 2 tipos de enemigos además de púas en el techo y piso, el daño es el mismo para todos.

- PlayerVars mantiene todas las variables propias del jugador, al sufrir daño HP disminuye, no así maxHP que es la variable a modificar desde servidor y desde DataManager.

- El nivel termina al perder toda la vida, llegar a tiempo 0 o salir de los márgenes del nivel, actualizando datos del usuario en servidor y cargando la escena MainMenu.

- Los controles de juegos son: Flechas direccionales (derecha/izquierda) para moverse y espacio para saltar.

- Las púas hacen daño cada cierto tiempo, los enemigos mientras se mantenga contacto con ellos.

- Los datos de ingreso al administrador web son: User: admin / Pass: adminadmin / [CPANEL](http://ardillaprogramadora.com/anich/app/index.php)