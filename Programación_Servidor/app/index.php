<?php
session_start();

if(isset($_SESSION["95_usuario_id"])){
	//echo "hay sesion";
}else{
	
	header("location: ../index.php");
	//echo "No hay sesion";
}

   require_once("model/Juego_model.php");
   $juego = new Juego_model();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Anich</title>

	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/juego.css">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js" ></script>
    <script src="js/juego.js" ></script>

</head>
<body>
	

<!-- Modal -->

			<?php 
			
				$juego_velocidad;
				$juego_tiempo;
				$juego_puntos;
				$juego_damage;
				$juego_vitalidad;

			foreach ($juego->get_data_juego() as $j) {
				$juego_velocidad=$j["jue_velocidad"];
				$juego_tiempo=$j["jue_tiempo_duracion"];
				$juego_puntos=$j["jue_puntos_item"];
				$juego_damage=$j["jue_dano"];
				$juego_vitalidad=$j["jue_vitalidad"];
			}

			?>

<div class="modal fade" id="ModalParam" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">MODIFICACION DE PARAMETROS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-3 col-form-label">Velocidad</label>
		    <div class="col-sm-9">
		      <input class="form-control form-control-lg" id="txt_velocidad" type="text" value="<?php echo $juego_velocidad; ?>">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-3 col-form-label">Tiempo lvl</label>
		    <div class="col-sm-9">
		      <input class="form-control form-control-lg" id="txt_tiempo" type="text" value="<?php echo $juego_tiempo; ?>">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-3 col-form-label">Puntos x Item</label>
		    <div class="col-sm-9">
		      <input class="form-control form-control-lg" id="txt_puntos" type="text" value="<?php echo $juego_puntos; ?>">
		    </div>
		  </div>

		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-3 col-form-label">Vitalidad</label>
		    <div class="col-sm-9">
		      <input class="form-control form-control-lg" id="txt_vitalidad" type="text" value="<?php echo $juego_vitalidad; ?>">
		    </div>
		  </div>


		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-3 col-form-label">Daño</label>
		    <div class="col-sm-9">
		      <input class="form-control form-control-lg" id="txt_damage" type="text" value="<?php echo $juego_damage; ?>">
		    </div>
		  </div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-primary" id="btn_mod_param">Guardar Cambios</button>
      </div>
    </div>
  </div>
</div>


<div id="pagina">
	
<div id="contenedor_main">
		
	<div class="row">
		<div class="col-6">
			<div class="titulo_seccion">
			USUARIOS REGISTRADOS
			</div>

			<div class="listado_contenedor">
				<table class="invd_tabla">
					<tr>
						<th>ID</th>
						<th>USUARIO</th>
						<th>PUNTAJE</th>

					</tr>

			<?php 
			foreach ($juego->listado_usuario() as $usu) {
				echo "<tr>
						<td>".$usu["usu_id"]."</td>
						<td>".$usu["usu_nickname"]."</td>
						<td>".$usu["usu_score"]."</td>

					</tr>";
				}
		 	?>



				</table>

			</div><!--listado_contenedor-->


		</div>
		<div class="col-6">

			<div class="titulo_seccion">
				
				PARAMETROS DE JUEGO
				<div class="btn_pu_parametros" data-toggle="modal" data-target="#ModalParam"><span class="icon-gear"></span></div>
			</div>


			<div class="parametro_contenedor">
				<div class="parametro">
					<div class="p_nombre">Velocidad</div>
					<div class="p_valor"><?php echo $juego_velocidad; ?></div>
				</div>

				<div class="parametro">
					<div class="p_nombre">Tiempo lvl</div>
					<div class="p_valor"><?php echo $juego_tiempo; ?></div>
				</div>

				<div class="parametro">
					<div class="p_nombre">Puntos x Item</div>
					<div class="p_valor"><?php echo $juego_puntos; ?></div>
				</div>	

				<div class="parametro">
					<div class="p_nombre">Vitalidad</div>
					<div class="p_valor"><?php echo $juego_vitalidad; ?></div>
				</div>	

				<div class="parametro">
					<div class="p_nombre">Daño</div>
					<div class="p_valor"><?php echo $juego_damage; ?></div>
				</div>				


			</div><!--parametro_contenedor-->



		</div>
	</div>

		





	</div>



</div><!--pagina-->




</body>
</html>