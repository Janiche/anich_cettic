$(document).ready(function(){


	// EVENTO CLICK DEL BOTON QUE GUARDA LOS CAMBIOS

	$("#btn_mod_param").on("click",function(){
		
		let velocidad = $("#txt_velocidad").val();
		let tiempo = $("#txt_tiempo").val();
		let puntos = $("#txt_puntos").val();
		let vitalidad = $("#txt_vitalidad").val();
		let damage = $("#txt_damage").val();

		// Mediante ajax, enviamos los parametros al archivo juego_ajax.php
		// que se encarga de llamar a la funcion que modifica los parametros
		// del juego.

		$.ajax({
		  beforeSend:function(){},
		  type: 'POST',
		  url: 'ajax/juego_ajax.php',
		  enctype: 'multipart/form-data',
		  data: {accion:"refresh_param",velocidad:velocidad,tiempo:tiempo,puntos:puntos,vitalidad:vitalidad,damage:damage},
		  success: function(result) {
		  	
		  	$(".parametro_contenedor").html(result);
		  	$("#ModalParam").modal("hide");

		  },
		  error: function(){},
		  complete:function(){
	    }
	   });
	});


});