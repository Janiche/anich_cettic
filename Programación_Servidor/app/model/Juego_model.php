<?php 

class Juego_model{

	private $db;

	public function __construct(){
		require_once("Conector_pg.php");
		$this->db=new Conector_pg();
	}


 	public function registro_usuario($nickname,$pass){

 		$sql="insert into usuario (usu_nickname,usu_pass,usu_score)
			  values('".$nickname."','".$pass."',0);";

		$select=$this->db->consulta_iud($sql);	 
		
		if($select=="ok"){ //Resource id #5
			return "ok";
		}else{
			return "error";
		}
 	}


 	public function existe_usuario($nickname){

 		$select=$this->db->consultar("select count(usu_id)as total from usuario where lower(usu_nickname) = lower('".$nickname."');");
		$fila=pg_fetch_array($select);
		return $fila["total"];
 	}


 	public function valida_login($nickname,$pass){

 		$select=$this->db->consultar("select count(usu_id) as total from usuario where lower(usu_nickname) = lower('".$nickname."') and usu_pass='".$pass."';");
		$fila=pg_fetch_array($select);
		return $fila["total"];
 	}


 	public function modifica_score($nickname,$puntos){

 		$select=$this->db->consulta_iud("update usuario set usu_score=".$puntos." where lower(usu_nickname) = lower('".$nickname."');");
		// $fila=pg_fetch_array($select);
		// return $fila["total"];
		return $select;
 	}

 	public function get_score($nickname){

 		$select=$this->db->consultar("select usu_score from usuario where lower(usu_nickname) = lower('".$nickname."');");
		$fila=pg_fetch_array($select);
		return $fila["usu_score"];
 	}

 	// RETORNAMOS LOS VALORES DEL JUEGO
 	public function get_data_juego(){
 		$sql="select
				jue_velocidad,
				jue_tiempo_duracion,
				jue_puntos_item,
				jue_dano,
				jue_vitalidad
				from juego;";

 		$select=$this->db->consultar($sql);
		
		$resultado=[];
		while ($fila=pg_fetch_array($select)) {
			$resultado[]=$fila;
		}
		return $resultado;
 	}

 	public function mod_data_juego($velocidad,$tiempo,$puntos,$damage,$vitalidad){
 		$sql="update juego set
			jue_velocidad = ".$velocidad.",
			jue_tiempo_duracion = ".$tiempo.",
			jue_puntos_item = ".$puntos.",
			jue_dano = ".$damage.",
			jue_vitalidad = ".$vitalidad.";";

 		$select=$this->db->consultar($sql);
		
		$resultado=[];
		while ($fila=pg_fetch_array($select)) {
			$resultado[]=$fila;
		}
		return $resultado;
 	}


	// LISTADO DE JUGADORES APP WEB
 	public function listado_usuario(){

 		$sql="select
				usu_id,
				usu_nickname, 
				usu_score
				from usuario order by usu_id;";

 		$select=$this->db->consultar($sql);
		
		$resultado=[];
		while ($fila=pg_fetch_array($select)) {
			$resultado[]=$fila;
		}
		return $resultado;

 	}



 }

 ?>