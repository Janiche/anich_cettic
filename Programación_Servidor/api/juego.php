<?php 

	require_once("../app/model/Juego_model.php");
	$juego = new Juego_model();

	$accion="";

	//asignamos un valor a la variable accion
	if(isset($_GET["accion"])){
			if($_GET["accion"]!=""){
				$accion = $_GET["accion"];
			}
	}



if($accion=="reg"){

		$valida_param=true;
		$nickname;
		$pass;


		if(isset($_GET["ni"])){
			$nickname=$_GET["ni"];
		}else{
			$valida_param=false;
		}

		if(isset($_GET["p"])){
			$pass=$_GET["p"];
		}else{
			$valida_param=false;
		}		

		if($valida_param){

			// validamos que no este registrado previamente
			$valida_u=$juego->existe_usuario($nickname);

			if( (int)$valida_u==0 ){

				$valida_reg=$juego->registro_usuario($nickname,$pass);
				$array = array("status"=>$valida_reg);
			}

			elseif ( (int)$valida_u!=0 ){
				$array = array("status"=>"Usuario registrado");
			}
			
			echo json_encode($array);
	

		}else{
			$array = array("status"=>"Faltan parametros");
			echo json_encode($array);
		}

		
	}// fin registro

	//modifica score
	elseif ($accion=="mod_score"){

		$valida_param=true;
		$nickname;
		$score;

		if(isset($_GET["ni"])){
			$nickname=$_GET["ni"];
		}else{
			$valida_param=false;
		}

		if(isset($_GET["sc"])){
			$score=$_GET["sc"];
		}else{
			$valida_param=false;
		}	

		if($valida_param){

			$valida_reg=$juego->modifica_score($nickname,$score);
			$array = array("status"=>$valida_reg);
			// $array = array("status"=>"ok");

		}else{
			$array = array("status"=>"Faltan parametros");
			
		}
			echo json_encode($array);


	}

	elseif ($accion=="login") {

		$valida_param=true;

		$juego_velocidad;
		$juego_tiempo;
		$juego_puntos;
		$juego_damage;
		$juego_vitalidad;

		$nickname;
		$pass;
		// validamos que se este enviando por get el nickname
		if(isset($_GET["ni"])){
			$nickname=$_GET["ni"];
		}else{
			$valida_param=false;
		}

		if(isset($_GET["p"])){
			$pass=$_GET["p"];
		}else{
			$valida_param=false;
		}	

		// si los parametros estan ok
		if($valida_param){

			//echo "("..")";
			$valida_log=$juego->valida_login($nickname,$pass);

			if((int)$valida_log!=0){

			foreach ($juego->get_data_juego() as $j) {
				$juego_velocidad=$j["jue_velocidad"];
				$juego_tiempo=$j["jue_tiempo_duracion"];
				$juego_puntos=$j["jue_puntos_item"];
				$juego_damage=$j["jue_dano"];
				$juego_vitalidad=$j["jue_vitalidad"];
			}

			$puntaje = $juego->get_score($nickname);

			$array = array("status"=>"ok",
				"juego_velocidad"=>$juego_velocidad,
				"juego_tiempo"=>$juego_tiempo,
				"juego_puntos"=>$juego_puntos,
				"juego_damage"=>$juego_damage,
				"juego_vitalidad"=>$juego_vitalidad,
				"puntaje"=>$puntaje);

			}else{
				$array = array("status"=>"Usuario o clave incorrectas");
			}



			

		}else{
			$array = array("status"=>"Faltan parametros");
		}

		echo json_encode($array);
		

	}

	elseif ($accion=="juego") {
		

		$juego_velocidad;
		$juego_tiempo;
		$juego_puntos;
		$juego_damage;
		$juego_vitalidad;


		// validamos que se este enviando por get el nickname

			foreach ($juego->get_data_juego() as $j) {
				$juego_velocidad=$j["jue_velocidad"];
				$juego_tiempo=$j["jue_tiempo_duracion"];
				$juego_puntos=$j["jue_puntos_item"];
				$juego_damage=$j["jue_dano"];
				$juego_vitalidad=$j["jue_vitalidad"];
			}


			$array = array(
				"juego_velocidad"=>$juego_velocidad,
				"juego_tiempo"=>$juego_tiempo,
				"juego_puntos"=>$juego_puntos,
				"juego_damage"=>$juego_damage,
				"juego_vitalidad"=>$juego_vitalidad);

			echo json_encode($array);
	


	}


?>