<?php 

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Anich</title>

	<style>
		
		@import url('https://fonts.googleapis.com/css?family=Cantarell:400,400i,700,700i');

		body{
			/*background-color: #4c56a3;*/
			font-family: 'Cantarell', sans-serif;
			background: url("app/img/1.png");
		}


		#contenedor_login{
			padding: 0;
			margin: 0;
			width: 100%;
			height: 100vh;
			display: flex;
			justify-content:center;
			align-items: center;
		}

		.fila{
			margin-top: 15px;
			margin-bottom: 15px;
			/*background-color: orange;*/
			display: flex;
			justify-content:center;

		}

		#login_juego{
			width: 500px;
			height: 400px;
			background-color: rgba(255,255,255,0.2);
			-moz-box-sizing: border-box;
			box-sizing: border-box;	
			padding: 20px;

			display: -ms-flexbox;
			display: -webkit-flex;
			display: flex;

			-webkit-flex-direction: row;
			-ms-flex-direction: row;
			flex-direction: row;

			-webkit-justify-content: center;
			-ms-flex-pack: justify;
			justify-content: center;

			-webkit-align-items: center;
			-ms-flex-align: center;
			align-items: center;


		}

		.logo{
			width: 100%;
		}

		.txt_login{
			width: 100%;
			height: 45px;
			border: 1px solid #FF530D;;
			border-radius: 5px;
			font-size: 16px;

			padding-left: 20px;

		}

		#btn_login{
			width: 250px;
			height: 45px;
			text-align: center;
			color: #fff;
			background-color: #FF530D;;
			border-radius: 5px;
			/*padding-top: 5px;*/
			display: flex;
			justify-content:center;
			align-items: center;
			-moz-box-sizing: border-box;
			box-sizing: border-box;	
			font-size: 16px;
			cursor: pointer;
		}
	
	</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		//alert("oli");

		$("#btn_login").on("click",function(){

			let valida = true;
			let usuario = $("#text_usuario").val();
			let clave = $("#text_clave").val();
			let mensaje = "Debes ingresar : ";

			if(usuario==""){
				mensaje +="usuario ";
				valida = false;
			}
			if(clave==""){
				mensaje +="clave ";
				valida = false;
			}

			if(valida){

			   $.ajax({
				  beforeSend:function(){},
				  type: 'POST',
				  url: 'login_procesa.php',
				  enctype: 'multipart/form-data',
				  data: {usuario:usuario,clave:clave},
				  success: function(result) {
				  	//alert(result);
				    if(result!="0" || result!=0){
				    	window.location.href = "app/index.php";
				    }else{
						alert("datos incorrectos ");
				    }
				  },
				  error: function(){},
				  complete:function(){
			    }
			   });

			}else{
				alert(mensaje);
			}

			//alert("hiii hahah");
		});
	});
</script>


</head>
<body>

<div id="contenedor_login">
	
	<div id="login_juego">
		<div>
			
		

			<div class="fila">
				<input class="txt_login" id="text_usuario" type="text" placeholder="user"> 
			</div>

			<div class="fila">
				<input class="txt_login" id="text_clave" type="password" placeholder="password">
			</div>

			<div class="fila">
				<div id="btn_login">Login</div>
			</div>

		</div>
		
	</div>

</div>




	
</body>
</html>