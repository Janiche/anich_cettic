﻿using System.Collections;
using UnityEngine;

using UnityEngine.SceneManagement;

public class SceneLoader 
{

    /// <summary>
    /// Carga de manera asincrona una escena
    /// </summary>
    /// <returns>The async scene.</returns>
    /// <param name="scene">Escena a cargar.</param>
    public static IEnumerator LoadAsyncScene(string scene, float wait)
    {
        Resources.UnloadUnusedAssets();

        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        async.allowSceneActivation=false;

        //SaveSystem.Save();
        while(!async.isDone)
        {
            //SaveSystem.Save();
            yield return new WaitForSeconds(wait);
            async.allowSceneActivation=true;
        }
        yield return 0;
    }

    /// <summary>
    /// Carga de manera asincrona una escena
    /// </summary>
    /// <returns>The async scene.</returns>
    /// <param name="scene">Indice de la escena a cargar.</param>
    public static IEnumerator LoadAsyncScene(int index, float wait=0)
    {
        Resources.UnloadUnusedAssets();

        AsyncOperation async = SceneManager.LoadSceneAsync(index);
        async.allowSceneActivation = false;

        while(!async.isDone)
        {
            //SaveSystem.Save();
            yield return new WaitForSeconds(wait);
            async.allowSceneActivation = true;
        }
        yield return 0;
    }
}
