﻿using UnityEngine;

public interface IDamageable<T>
{
    void GetDamage(T damage);
}
