﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : Obstacle
{
    [SerializeField] protected bool coldDown = true;

    protected void OnCollisionExit2D(Collision2D col)
    {
        coldDown = false;
        StopCoroutine("Coldown");
    }

    protected void OnCollisionEnter2D(Collision2D col)
    {
        coldDown = true;
        //StartCoroutine("Coldown");
    }

    protected override void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (coldDown)
            {
                MakeDamage();
                coldDown = false;
                StartCoroutine("Coldown");
            }
        }
    }

    /// <summary>
    /// Activa "temporizador" para volver a dañar
    /// </summary>
    /// <returns>The coldown.</returns>
    protected IEnumerator Coldown()
    {
        yield return new WaitForSeconds(2f);
        coldDown = true;
    }
}
