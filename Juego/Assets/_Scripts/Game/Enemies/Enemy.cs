﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Obstacle
{
    [SerializeField] Transform tempDest = null;
    [SerializeField] private List<Transform> destination = new List<Transform>();
    [SerializeField] private float movementSpeed = 8;
    [SerializeField] private float distance = 0.1f;
    [SerializeField] private AudioSource spinFx = null;
    private bool moving = false;
    private int index = 0;

    private void OnEnable()
    {
        if (destination.Count > 0)
        {
            transform.position = destination[0].position;
        }
    }

    protected override void OnCollisionStay2D(Collision2D col)
    {
        MakeDamage();
    }

    private void FixedUpdate()
    {
        Movement();
    }

    /// <summary>
    /// Encargada de mover al enemigo por ruta
    /// </summary>
    private void Movement()
    {
        //ASEGURA QUE LA RUTA TENGA AL MENOS 2 PUNTOS DE DESTINO
        if (destination.Count > 1)
        {

            if (!moving)
            {
                if (index < destination.Count - 1)
                {
                    index++;
                }
                else
                {
                    index = 0;
                }

                tempDest = destination[index];
                moving = true;
            }

            else
            {
                if (Vector2.Distance(transform.position, tempDest.position) > distance)
                {
                    float step = movementSpeed * Time.fixedDeltaTime;
                    transform.position = Vector2.MoveTowards(transform.position, tempDest.position, step);
                }
                else
                {
                    moving = false;
                }
            }
        }
    }
}
