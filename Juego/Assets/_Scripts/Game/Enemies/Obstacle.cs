﻿using System.Collections;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private PlayerVars vars;
    [Tooltip("Variable de cuanto daño hacer")] public float damage = 5;


    protected virtual void OnCollisionStay2D(Collision2D col)
    {
    }

    protected void MakeDamage()
    {
        vars.hp -= damage;

        vars.hp = Mathf.Clamp(vars.hp, 0, vars.maxHp);

        GameManager.Instance.UpdateDamage();
    }
}
