﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance
    {
        get { return instance; }
    }

    public delegate void GameEvent();
    public GameEvent OnPlayerDie;

    public void PlayerDie()
    {
        if (OnPlayerDie != null)
        {
            OnPlayerDie();
        }
    }

    [Header("Variables de Jugador")]
    [SerializeField] private PlayerVars vars = null;

    [Tooltip("Barra de HP de jugador")] [SerializeField] private Image playerHpBar = null;
    [Tooltip("Texto de score de jugador")] [SerializeField] private TextMeshProUGUI scoreText = null;


    [Header("Variables de Temporizador")]
    [SerializeField] private TextMeshProUGUI timerText = null;
    [SerializeField] private float totalTime = 30f;
    [SerializeField] private float remainingTime = 30;

    [SerializeField] private UnityEvent EndGame;
    private bool sceneLoaded = false;

    private void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        OnPlayerDie += onPlayerDie;

        //SE ESTABLECEN LOS VALORES INICIALES PARA EL JUGADOR
        //SOLO SI LA INSTANCIA DE DATAMANAGER EXISTE REEMPLAZA DATOS
        if (DataManager.Instance != null)
        {
            DataManager.Instance.ReplaceData(out remainingTime);
        }
        //EN CASO QUE NO EXISTA, ASIGNA VALORES INTERNOS
        else
        {
            remainingTime = totalTime;
            vars.hp = vars.maxHp;
        }

    }

    private void OnDisable()
    {
        OnPlayerDie -= onPlayerDie;
    }

    void Update()
    {
        //ASEGURA INSTANCIA CREADA ANTES DE INICIAR CONTADOR
        if (instance != null)
        {
            if (remainingTime > 0)
            {
                remainingTime -= Time.deltaTime;
            }
            else
            {
                vars.hp = 0;
                PlayerDie();
            }
            timerText.text = remainingTime.ToString("00");
        }
    }

    /// <summary>
    /// Actualiza la UI de vida del personaje
    /// </summary>
    public void UpdateDamage()
    {
        playerHpBar.fillAmount = vars.hp / vars.maxHp;
        playerHpBar.fillAmount = Mathf.Clamp01(playerHpBar.fillAmount);
    }

    public void UpdateScore()
    {
        scoreText.text = vars.score.ToString("00");
    }

    private void onPlayerDie()
    {
        if (!sceneLoaded)
        {
            EndGame.Invoke();
            DataManager.Instance.UpdateScore();
            sceneLoaded = true;
        }
    }
}
