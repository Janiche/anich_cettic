﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private PlayerVars vars = null;
    [SerializeField] private AudioSource pickupFx = null;
    public int scoreValue = 10;
    private bool active = true;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (active)
            {
                active = false;
                vars.score += scoreValue;
                GameManager.Instance.UpdateScore();
                pickupFx.Play();

                //DESACTIVA VISUAL Y FUNCIONAL MONEDA
                GetComponent<CircleCollider2D>().enabled = false;
                GetComponent<SpriteRenderer>().enabled = false;
                //gameObject.SetActive(false);
            }
        }
    }
}
