﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] Transform tempDest = null;
    [SerializeField] private List<Transform> destination = new List<Transform>();
    [SerializeField] private float movementSpeed = 8;
    [SerializeField] private float distance = 0.5f;
    [SerializeField] private Rigidbody2D rb = null;
    private bool moving = false;
    private int index = 0;


    private void FixedUpdate()
    {
        Movement();
    }

    /// <summary>
    /// Encargada de mover la plataforma por ruta
    /// </summary>
    private void Movement()
    {
        //ASEGURA QUE LA RUTA TENGA AL MENOS 2 PUNTOS DE DESTINO
        if (destination.Count > 1)
        {

            if (!moving)
            {
                if (index < destination.Count - 1)
                {
                    index++;
                }
                else
                {
                    index = 0;
                }

                tempDest = destination[index];
                moving = true;
            }

            else
            {
                //VALIDA SI LLEGÓ AL PUNTO OBJETIVO
                if (Vector2.Distance(transform.position, tempDest.position) > distance)
                {
                    float step = movementSpeed * Time.fixedDeltaTime;

                    //MUEVE LA PLATAFORMA UTILIZANDO RIGIDBODY
                    Vector3 movePos = Vector2.MoveTowards(transform.position, tempDest.position, step);
                    rb.MovePosition(movePos);
                }
                else
                {
                    moving = false;
                }
            }
        }
    }
}
