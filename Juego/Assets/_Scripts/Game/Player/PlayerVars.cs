﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVars : MonoBehaviour
{
    [Tooltip("Variable de vida máxima del personaje (EDITADA POR SERVIDOR)")] public float maxHp = 100;
    [Tooltip("Variable de vida actual del personaje")] public float hp = 100;
    [Tooltip("Nombre del usuario necesario para actualizar puntaje")] public string user = "";
    [Tooltip("Variable de puntaje de jugador")] public int score = 0;
}
