﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, IDamageable<float>
{
    [Header("Variables de Jugador")]
    [SerializeField] private PlayerVars vars = null;

    [Header("Variables de Movimiento")]
    [Tooltip("Valor de desplazamiento horizontal")] [SerializeField] private float horizontal = 0;
    [Tooltip("Valor de desplazamiento vertical")] [SerializeField] private float vertical = 0;
    [Tooltip("Velocidad de desplazamiento")] [SerializeField] private float movementSpeed = 3f;
    [Tooltip("Fuerza de salto")] [SerializeField] private float jumpForce = 3f;
    [Tooltip("Variable de Salto")] [SerializeField] private bool jump = false;
    [SerializeField] private Animator animator = null;
    [SerializeField] private Rigidbody2D rb = null;
    private bool onAir = false;

    private void Update()
    {
        //OBTIENE VALORES DE INPUT
        horizontal = Input.GetAxis("Horizontal") * movementSpeed;

        if (!onAir)
        {
            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
            }
        }

        //SI VIDA ES MAYOR A 0 PERMITE CAMINATA
        if (vars.hp > 0)
        {
            animator.SetFloat("Walk", Mathf.Abs(horizontal));
        }

        //DE LO CONTRARIO EL PERSONAJE MUERE
        else
        {
            animator.SetTrigger("Death");
            GameManager.Instance.PlayerDie();
        }
    }

    private void FixedUpdate()
    {
        //SI LA VIDA ES MAYOR A 0 PERMITE CONTROL
        if (vars.hp > 0)
        {
            Movement();

            if (jump && !onAir)
            {
                Jump();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Floor"))
        {
            animator.SetBool("Jump", jump);
            onAir = false;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Scene"))
        {
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
            rb.isKinematic = true;
            vars.hp = 0;
            GameManager.Instance.PlayerDie();
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Permite el movimiento del personaje en función de la velocidad y el input
    /// </summary>
    private void Movement()
    {
        transform.Translate(transform.right * horizontal * Time.fixedDeltaTime);

        //CREA ANGULO DE DIRECCIÓN AL CUAL MIRAR
        float angle = (horizontal >= 0) ? 0 : 180;
        transform.eulerAngles = new Vector3(0, angle, 0);

    }

    /// <summary>
    /// Permite al personaje saltar
    /// </summary>
    private void Jump()
    {
        animator.SetBool("Jump", jump);
        onAir = true;
        jump = false;

        rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }

    /// <summary>
    /// Agrega daño al personaje
    /// </summary>
    /// <param name="damage">Daño a recibir.</param>
    public void GetDamage(float damage)
    {
        vars.hp = ((vars.hp - damage) > 0) ? (vars.hp - damage) : 0;
        GameManager.Instance.UpdateDamage();
    }
}
