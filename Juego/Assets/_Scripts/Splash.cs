﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour
{
    private void Awake()
    {
        StartCoroutine(SceneLoader.LoadAsyncScene("MainMenu", 0));
    }
}
