﻿using UnityEngine;
using UnityEngine.Events;

using System.Text.RegularExpressions;

using TMPro;
using SimpleJSON;

public class Buttons : MonoBehaviour
{
    private const string regex = "^[_A-z0-9]*([_A-z0-9])*$";
    [Header("Input de Datos de usuario")]
    [SerializeField] private TMP_InputField userInput = null;
    [SerializeField] private TMP_InputField passInput = null;

    [Header("Mensajes de error")]
    [SerializeField] private GameObject requiredError = null;
    [SerializeField] private GameObject badDataError = null;
    [SerializeField] private GameObject noDataError = null;
    [SerializeField] private GameObject duplicatedUserError = null;
    [SerializeField] private GameObject specialCharError = null;

    public UnityEvent OnLogin;
    public UnityEvent OnLogout;
    RequestManager requestManager;

    private void OnEnable()
    {
        //CREA ENCARGADO DE LAS PETICIONES A SERVIDOR
        requestManager = new RequestManager("ardillaprogramadora.com/anich/api/juego.php?accion=");

        //NO HAY SESIÓN INICIADA
        if (!DataManager.Instance.IsLogged())
        {
            OnLogout.Invoke();
        }

        //HAY SESIÓN INICIADA
        else
        {
            OnLogin.Invoke();
        }

        //INICIA LA PETICIÓN DE DATOS
        StartCoroutine(requestManager.request("juego", DataManager.Instance.GetDataCallback));
    }

    /// <summary>
    /// Inicia sesión del usuario ingresado
    /// </summary>
    public void Login()
    {
        string user = userInput.text;
        string pass = passInput.text;

        //NO EXISTEN DATOS INGRESADOS
        if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(pass))
        {
            requiredError.SetActive(true);
            return;
        }

        if (!Regex.IsMatch(user, regex) || !Regex.IsMatch(pass, regex))
        {
            specialCharError.SetActive(true);
            return;
        }

        StartCoroutine(requestManager.request("login&ni=" + user + "&p=" + pass, LoginCallback));

    }

    public void Logout()
    {
        DataManager.Instance.UpdateUser("", 0);
        OnLogout.Invoke();
    }

    /// <summary>
    /// Inicia el registro de usuario
    /// </summary>
    public void Register()
    {
        string user = userInput.text;
        string pass = passInput.text;

        //NO EXISTEN DATOS INGRESADOS
        if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(pass))
        {
            requiredError.SetActive(true);
            return;
        }

        StartCoroutine(requestManager.request("reg&ni=" + user + "&p=" + pass, RegisterCallback));

    }

    public void PlayGame()
    {
        StartCoroutine(SceneLoader.LoadAsyncScene("Game", 0));
    }

    private void LoginCallback(WWW www)
    {
        var json = JSON.Parse(www.text);

        string status = json["status"];

        switch (status.ToLower())
        {
            //LOGIN CORRECTO
            case "ok":
                DataManager.Instance.UpdateUser(userInput.text, int.Parse(json["puntaje"].Value));

                OnLogin.Invoke();
                break;

            //ERROR AL LOGIN
            case "usuario o clave incorrecta":
                badDataError.SetActive(true);
                break;

            default:
                noDataError.SetActive(true);
                break;
        }

        Debug.LogError(json);
    }

    private void RegisterCallback(WWW www)
    {
        var json = JSON.Parse(www.text);

        string status = json["status"];

        switch (status.ToLower())
        {
            case "usuario registrado":
                duplicatedUserError.SetActive(true);
                break;

            case "faltan parametros":
                requiredError.SetActive(true);
                break;

            case "ok":
                DataManager.Instance.UpdateUser(userInput.text, 0);
                OnLogin.Invoke();
                break;

            case "error":
                break;
        }
        Debug.LogError(json);
    }
}
