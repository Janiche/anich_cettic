﻿using UnityEngine;
using System.Collections;

public class RequestManager
{
    private string basePath = "";

    public RequestManager()
    {

    }

    public RequestManager(string _path)
    {
        basePath = _path;
    }

    public IEnumerator request(string route, System.Action<WWW> callback)
    {
        string url = this.basePath + route;

        WWW www = new WWW(url);

        yield return www;

        callback(www);
    }
    //
    //    public IEnumerator request(string url, System.Action<WWW> callback)
    //    {
    ////        string url = this.basePath + route;
    //
    //        Debug.LogWarning(url);
    //
    //        WWW www = new WWW(url);
    //
    //        yield return www;
    //
    //        callback(www);
    //    }

    public IEnumerator request(string url, string route, System.Action<WWW> callback)
    {
        url = basePath + route;

        //Debug.LogWarning(url);

        WWW www = new WWW(url);

        yield return www;

        callback(www);
    }
}
