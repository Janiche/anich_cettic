﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

using SimpleJSON;

public class DataManager : MonoBehaviour
{
    private static DataManager instance = null;
    public static DataManager Instance
    {
        get { return instance; }
    }

    [Header("Variables de conexión")]
    [Tooltip("Tiempo necesario para volver a obtener datos (seg(")]
    [SerializeField] private float dataTime = 15;

    [Header("Variables de juego")]
    [SerializeField] private float playerHP = 100;
    [SerializeField] private float playerSpeed = 4;
    [SerializeField] private float gameTime = 40;
    [SerializeField] private int itemScore = 10;
    [SerializeField] private float enemyDamage = 50;

    [Header("Variables propias de jugador")]
    [SerializeField] private int currentScore = 0;
    [SerializeField] private PlayerVars vars = null;

    [Header("Variables de objetos y Enemigos")]
    [SerializeField] private List<Obstacle> obstacles = new List<Obstacle>();
    [SerializeField] private Coin coin = null;
    RequestManager requestManager;

    private void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            requestManager = new RequestManager("ardillaprogramadora.com/anich/api/juego.php?accion=");
        }
        else
        {
            Destroy(gameObject);
        }

        //RESETEA VALORES
        vars.user = "";
    }

    public bool IsLogged()
    {
        //Si es nulo o vacio retorna false
        bool result = !string.IsNullOrEmpty(vars.user);

        return result;
    }

    /// <summary>
    /// Sobreescribe los datos de PlayerVars por los obtenidos
    /// desde servidor
    /// </summary>
    /// <param name="totalTime">Tiempo total de juego.</param>
    public void ReplaceData(out float totalTime)
    {
        //REEMPLAZA DATOS DEL JUGADOR
        vars.maxHp = playerHP;
        vars.hp = playerHP;
        //vars.score = currentScore;

        //REEMPLAZA DATOS DE JUEGO
        totalTime = gameTime;

        //REEMPLAZA DATOS DE OBJETOS
        coin.scoreValue = itemScore;

        //REEMPLAZA DATOS DE ENEMIGOS
        for (int i = 0; i < obstacles.Count; i++)
        {
            obstacles[i].damage = enemyDamage;
        }
        GameManager.Instance.UpdateScore();

        StopCoroutine("GetData");
    }

    /// <summary>
    /// Actualiza los datos del jugador
    /// </summary>
    /// <param name="ni">Nombre de Usuario.</param>
    /// <param name="score">Puntaje del jugador.</param>
    public void UpdateUser(string ni, int score)
    {
        vars.user = ni;
        vars.score = score;
    }

    /// <summary>
    /// Obtiene información del servidor cada dataTime Segundos
    /// </summary>
    /// <returns>The data.</returns>
    public IEnumerator GetData()
    {
        yield return new WaitForSecondsRealtime(dataTime);
        StartCoroutine(requestManager.request("juego", GetDataCallback));
    }

    /// <summary>
    /// Actualiza el puntaje en servidor
    /// </summary>
    public void UpdateScore()
    {
        StartCoroutine(requestManager.request("mod_score&ni=" + vars.user + "&sc=" + vars.score, UpdateScoreCallback));
    }

    public void GetDataCallback(WWW www)
    {
        var json = JSON.Parse(www.text);

        Debug.LogError(json);
        playerSpeed = float.Parse(json["juego_velocidad"].Value);
        gameTime = float.Parse(json["juego_tiempo"].Value);
        itemScore = int.Parse(json["juego_puntos"].Value);
        enemyDamage = float.Parse(json["juego_damage"].Value);
        playerHP = float.Parse(json["juego_vitalidad"].Value);

        StartCoroutine("GetData");
    }

    private void UpdateScoreCallback(WWW www)
    {
        var json = JSON.Parse(www.text);

        string status = json["status"];

        switch (status.ToLower())
        {
            case "ok":
                StartCoroutine(SceneLoader.LoadAsyncScene("MainMenu", 1));
                break;

            default:
                break;
        }
    }
}
